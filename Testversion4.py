# In this version I visualised everything
import pygame
import random

pygame.init()
pygame.font.init()

black = (0, 0, 0)

screen = pygame.display.set_mode((0, 0), pygame.RESIZABLE)
pygame.display.set_caption('Guess The Number!')

icon = pygame.image.load("haticon.jpg")
pygame.display.set_icon(icon)

Background = pygame.image.load("Background.jpg")
Background = pygame.transform.scale(Background, (1300, 700))

intro = pygame.font.SysFont('Comic Sans MS', 25)
intro1 = intro.render('Welcome to Guess The Number!', False, black)
intro2 = intro.render('You have to guess the number that we have in mind.', False, black)
intro3 = intro.render("If you guess wrong we'll help you and tell you if it's too high or too low.", False, black)
intro4 = intro.render('If you guess right you may choose to stop and keep the money or to play again to win $5 more.', False, black)
question = intro.render("What do you guess? ", False, black)
answer = intro.render("", False, black)

Hat = pygame.image.load("Hat.png")
Hat = pygame.transform.scale(Hat, (130, 150))

number = random.randint(0, 100)
condition = True

while condition:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            condition = False

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_0:
                pass

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_0:
                answer = intro.render("0", False, black)
            if event.key == pygame.K_1:
                answer = intro.render("1", False, black)
            if event.key == pygame.K_2:
                answer = intro.render("2", False, black)
            if event.key == pygame.K_3:
                answer = intro.render("3", False, black)
            if event.key == pygame.K_4:
                answer = intro.render("4", False, black)
            if event.key == pygame.K_5:
                answer = intro.render("5", False, black)
            if event.key == pygame.K_6:
                answer = intro.render("6", False, black)
            if event.key == pygame.K_7:
                answer = intro.render("7", False, black)
            if event.key == pygame.K_8:
                answer = intro.render("8", False, black)
            if event.key == pygame.K_9:
                answer = intro.render("9", False, black)

    screen.fill(black)
    screen.blit(Background, (40, 0))

    screen.blit(intro1, (500, 200))
    screen.blit(intro2, (390, 240))
    screen.blit(intro3, (280, 280))
    screen.blit(intro4, (130, 320))
    screen.blit(question, (580, 530))
    screen.blit(answer, (820, 530))

    screen.blit(Hat, (60, 0))
    screen.blit(Hat, (200, 0))
    screen.blit(Hat, (340, 0))
    screen.blit(Hat, (480, 0))
    screen.blit(Hat, (620, 0))
    screen.blit(Hat, (760, 0))
    screen.blit(Hat, (900, 0))
    screen.blit(Hat, (1040, 0))
    screen.blit(Hat, (1180, 0))

    pygame.display.update()
