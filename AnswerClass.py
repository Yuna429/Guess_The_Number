# In this version I visualised everything
import pygame
import random

pygame.init()
pygame.font.init()

black = (0, 0, 0)

screen = pygame.display.set_mode((0, 0), pygame.RESIZABLE)
pygame.display.set_caption('Guess The Number!')

Background = pygame.image.load("Background.jpg")
Background = pygame.transform.scale(Background, (1300, 700))

intro = pygame.font.SysFont('Comic Sans MS', 25)
question = intro.render("What do you guess? ", False, black)

x = 820
mylist = ["_ ", "_ ", "_ "]
a = ''.join(mylist)
answer = intro.render(a, False, black)

condition = True

while condition:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            condition = False

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_0:
                mylist[0] = "0"
                #if x == 820 and a == 0 or 1 or 2 or 3 or 4 or 5 or 6 or 7 or 9:
                    #x = 830
            if event.key == pygame.K_1:
                a = "1"
            if event.key == pygame.K_2:
                a = "2"
            if event.key == pygame.K_3:
                a = "3"
            if event.key == pygame.K_4:
                a = "4"
            if event.key == pygame.K_5:
                a = "5"
            if event.key == pygame.K_6:
                a = "6"
            if event.key == pygame.K_7:
                a = "7"
            if event.key == pygame.K_8:
                a = "8"
            if event.key == pygame.K_9:
                a = "9"

    screen.fill(black)
    screen.blit(Background, (40, 0))

    screen.blit(question, (580, 530))
    screen.blit(answer, (x, 530))

    pygame.display.update()
